import React from 'react';
import './App.css';
//import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';


function App(props) {
  return (
    <div className="container">
      <LocationForm />
      {/* <AttendeesList attendees={props.attendees} /> */}
    </div>
  );
}

export default App;
