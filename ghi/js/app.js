

function createCard(name, description, pictureUrl, location, dates, dated) {
    return `

    <div class="card">
      <div>
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted"> <FONT COLOR="purple">${location}</h6></font>
            <p class="card-text">${description}</p>
          </div>
        </div>
        <div class="card-footer">
        ${dates} - ${dated}
      </div>

    </div>
    `;
}

function errorAlert() {
  return `
  <div class="alert alert-dark" role="alert">
  Something has gone wrong
  </div>
  `
}




window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
          const error = document.querySelector('#errors');
          error.innerHTML += errorAlert();



        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();



        //some kind of statement saying only a few columns for so many conferences
        const idList = ['#col1', '#col2', '#col3'];
        let i = 0;


        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const starts = details.conference.starts;
            const dates = new Date(starts).toLocaleDateString();
            const ends = details.conference.ends;
            const dated = new Date(ends).toLocaleDateString();
            const html = createCard(name, description, pictureUrl, location, dates, dated);
            const column = document.querySelector(idList[i%3]);
            column.innerHTML += html;
            i++;


          }
        }
      }
    } catch (e) {
      const error = document.querySelector('#errors');
      error.innerHTML += errorAlert();
    }
  });
