window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = option.value;
            selectTag.appendChild(option)
        }

    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        const formData = new FormData(formTag);                            //getting data from the form using the tag
        const json = JSON.stringify(Object.fromEntries(formData));        //creating a string from the form data

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            },

        };

        const response = await fetch(conferenceUrl, fetchConfig);  //Something wrong with this


        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference);
        //THIS ABOVE IS SO THAT THE FORM CLEARS AFTER BEING SUBMITTED
        //SO YOU KNOW SOMETHING HAS HAPPENED \^.^/
        }

    })
})
